# crypto-back
------------

API que permite obtener la información de la fluctuación de BTC y ETH, seleccionando periodos de tiempos por hora y mes.

### Node

[Node](http://nodejs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v10.6.0

    $ npm --version
    6.1.0

## Install

    $ git clone https://gitlab.com/aguileramarco.n/crypto-back.git
    $ cd crypto-back

## Redis Install

    $ brew install redis

- Redis start
```
brew services start redis
```

- Redis test server is running.
```
redis-cli ping
```

## Backend Running

Install dependencies

    $ npm install --production
`or`

    $ yarn

## Starting Local

To start project

    $ npm run start
`or`

    $ yarn start

    $ yarn dev

## Starting with Docker-compose
------------

To build project

    $ docker-compose build

To start project

    $ docker-compose up -d

To delete project

    $ docker-compose rm

## Is running
------------
```
curl -X GET \
  http://localhost:3001/ 
```
Response

```
  "API is working!"
```

## Endpoints

    $ GET /functionOptions

    $ GET /symbolOptions

    $ GET /marketOptions

    $ GET /currencyHistory?{queryParams}

```
queryParams example:
  functions: DIGITAL_CURRENCY_MONTHLY
  symbol: BTC
  market: USD
  dateMin: 22/7/2018
  dateMax: 25/7/2018
```