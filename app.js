const http = require('http');

const { logger, config } = require('./lib');
const { routes } = require('./routes');

const server = http.createServer();

server.on('request', routes.handleRequest);

// START SERVER
const { PORT } = config;
server.listen(PORT);

logger.info(`Starting in ${process.env.NODE_ENV} mode`);
logger.info(`Listening on ${PORT}`);

module.exports = server;
