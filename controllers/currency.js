const { logger, cache } = require('../lib');
const { currencyModel } = require('../models');

function parseResponse(list, keyFilter) {
  const responseData = [];
  Object.keys(list).forEach((key) => {
    const date = list[key];
    const value = date[keyFilter];
    const obj = {};
    obj[key] = value;
    responseData.push(obj);
  });
  return responseData;
}

module.exports = {
  getFunctionOptions: () => currencyModel.getFunctionOptions(),
  getSymbolOptions: () => currencyModel.getSymbolOptions(),
  getMarketOptions: () => currencyModel.getMarketOptions(),
  getCurrencyHistory: async (queryParams) => {
    const data = await currencyModel.getCurrencyHistory(queryParams);
    return data;
  },
  getCurrencyHistoryDaily: async (queryParams) => {
    let data = await cache.get(queryParams);

    if (data) {
      logger.info('Found Cache');
      return data;
    }
    logger.info('Cache not found');
    data = await currencyModel.getCurrencyHistory(queryParams);

    const { market } = queryParams;
    let responseData = [];

    try {
      const responseDaily = data['Time Series (Digital Currency Intraday)'];
      responseData = parseResponse(responseDaily, `1a. price (${market})`);
      cache.setDayCache(queryParams, responseData);
    } catch (error) {
      logger.error(`Error on getCurrencyHistoryDaily: ${error}`);
    }
    return responseData;
  },
  getCurrencyHistoryMonthly: async (queryParams) => {
    let data = await cache.get(queryParams);

    if (data) {
      logger.info('Found Cache');
      return data;
    }

    logger.info('Cache not found');
    data = await currencyModel.getCurrencyHistory(queryParams);

    const { market } = queryParams;
    let responseData = [];

    try {
      const responseMonthly = data['Time Series (Digital Currency Monthly)'];
      responseData = parseResponse(responseMonthly, `4a. close (${market})`);
      cache.setMonthCache(queryParams, responseData);
    } catch (error) {
      logger.error(`Error on getCurrencyHistoryMonthly: ${error}`);
    }
    return responseData;
  },
};
