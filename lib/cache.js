const redis = require('redis');
const logger = require('./logger');
const { REDIS } = require('./config');

// START REDIS CACHE
const { PORT, HOST } = REDIS;
const cache = redis.createClient(PORT, HOST);

cache.on('connect', () => {
  logger.info('Redis is now connected !');
});

cache.on('error', (err) => {
  logger.error(`Redis error: ${err}`);
});

module.exports = {
  get: key => new Promise((resolve) => {
    logger.info('Finding in cache');

    const { functions, symbol, market } = key;
    const keyToGet = `key:${functions}:${symbol}:${market}`;
    let result = null;
    cache.get(keyToGet, (err, results) => {
      if (!err && results !== null) result = JSON.parse(results);
      resolve(result);
    });
  }),

  setDayCache: (key, value) => {
    const { functions, symbol, market } = key;
    const { DAY_EXPIRE_TIME } = REDIS;
    const keyToSave = `key:${functions}:${symbol}:${market}`;

    logger.info(`Saving daily cache data: ${keyToSave}`);

    cache.set(keyToSave, JSON.stringify(value));
    cache.expire(keyToSave, DAY_EXPIRE_TIME);
  },

  setMonthCache: (key, value) => {
    const { functions, symbol, market } = key;
    const { MONTH_EXPIRE_TIME } = REDIS;
    const keyToSave = `key:${functions}:${symbol}:${market}`;

    logger.info(`Saving monthly cache data: ${keyToSave}`);

    cache.set(keyToSave, JSON.stringify(value));
    cache.expire(keyToSave, MONTH_EXPIRE_TIME);
  },
};
