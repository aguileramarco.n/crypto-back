require('../settings');

const nconf = require('nconf');

nconf.env();
const envvars = nconf.get('APP');
const config = (typeof envvars === 'string') ? JSON.parse(nconf.get('APP')) : envvars;

module.exports = config;
