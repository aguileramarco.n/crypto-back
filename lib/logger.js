const { createLogger, format, transports } = require('winston');

const { LOGGER } = require('./config');

const { combine, timestamp, printf } = format;

const myFormat = printf(info => `[${info.timestamp}] ${info.level}: ${info.message}`);

const logger = createLogger({
  level: LOGGER.LEVEL,
  format: combine(
    timestamp(),
    myFormat,
  ),
  transports: [new transports.Console()],
});

module.exports = logger;
