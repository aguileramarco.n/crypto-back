const moment = require('moment');

function filterMinData(data, minDate) {
  return data.filter((element) => {
    const key = Object.keys(element);
    return moment(key, 'YYYY-MM-DD HH:mm:ss').isAfter(minDate);
  });
}

function filterRangeData(data, minDate, maxDate) {
  return data.filter((element) => {
    const key = Object.keys(element);
    return (moment(key, 'YYYY-MM-DD HH:mm:ss').isAfter(minDate)
    && moment(key, 'YYYY-MM-DD HH:mm:ss').isBefore(maxDate));
  });
}

module.exports = {
  filterResponse: (data, filter, functions) => {
    let responseData;
    const min = moment(filter.min, 'DD/MM/YYYY');
    const max = moment(filter.max, 'DD/MM/YYYY');
    const validRange = min.isBefore(max);

    const timeStamp = new Date().getTime();
    const timeStampYesterday = timeStamp - (24 * 3600 * 1000);
    const timeStampLastYear = timeStamp - (365 * 24 * 3600 * 1000);

    if (functions === 'INTRADAY') {
      if (validRange) {
        responseData = filterRangeData(data, min, max);
      } else {
        // SHOULD RETURN PAST 24 HOURS
        const dateYesterday = new Date(timeStampYesterday);
        responseData = filterMinData(data, dateYesterday);
      }
    }
    if (functions === 'MONTHLY') {
      if (validRange) {
        responseData = filterRangeData(data, min, max);
      } else {
        // SHOULD RETURN PAST 24 HOURS
        const dateLastYear = new Date(timeStampLastYear);
        responseData = filterMinData(data, dateLastYear);
      }
    }
    return responseData;
  },
};
