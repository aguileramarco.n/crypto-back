const axios = require('axios');
const axiosRetry = require('axios-retry');
const retry = require('async-retry');

const { logger, config } = require('../lib');

const {
  BASE_URL, API_KEY, MAX_RETRIES, TIMEOUT,
} = config.API;

const alphaConfig = {
  timeout: TIMEOUT,
  baseURL: BASE_URL,
};

const alphaRequest = axios.create(alphaConfig);
axiosRetry(alphaRequest, { retries: MAX_RETRIES });

const getRequest = async params => retry(async () => {
  // Acid's example code to simulate and error
  if (Math.random(0, 1) < 0.1) {
    logger.error('How unfortunate! The API Request Failed, trying...');
    throw new Error('How unfortunate! The API Request Failed');
  }

  const { data } = await alphaRequest.get('/query', {
    params: {
      ...params,
    },
  });

  if (data['Error Message']) {
    logger.error('The External API response error, trying...');
    throw new Error('The External API response error');
  }

  return data;
}, { retries: 10 });

module.exports = {
  getFunctionOptions: () => {
    const data = [
      'DIGITAL_CURRENCY_INTRADAY',
      'DIGITAL_CURRENCY_DAILY',
      'DIGITAL_CURRENCY_WEEKLY',
      'DIGITAL_CURRENCY_MONTHLY',
    ];
    return data;
  },
  getSymbolOptions: () => {
    const data = [{
      name: 'Bitcoin',
      value: 'BTC',
    }, {
      name: 'Ethereum',
      value: 'ETH',
    }];
    return data;
  },
  getMarketOptions: () => {
    const data = [{
      name: 'Chilean Peso',
      value: 'CLP',
    }, {
      name: 'Chinese Yuan',
      value: 'CNY',
    }, {
      name: 'Euro',
      value: 'EUR',
    }, {
      name: 'United States Dollar',
      value: 'USD',
    }];
    return data;
  },
  getCurrencyHistory: async (queryParams) => {
    const { functions, symbol, market } = queryParams;
    const params = {
      function: functions,
      symbol,
      market,
      apikey: API_KEY,
    };

    const data = await getRequest(params);
    return data;
  },
};
