const url = require('url');
const { logger, utils } = require('../lib');
const { currency } = require('../controllers');

const API_WORKING = 'API is working!';
const API_NOT_FOUND = 'Path Not Found';
const API_BAD_REQUEST = 'Bad Request!';
const API_BAD_PARAMS = 'Parameters is invalid or missing';

const responseData = (data, response) => {
  response.write(JSON.stringify(data));
  response.end();
};

const validateParams = query => (query.functions && query.symbol && query.market);

const validateFilters = (query) => {
  let filter = {};
  if (query.dateMin || query.dateMax) {
    filter = {
      min: query.dateMin,
      max: query.dateMax,
    };
    delete query.dateMin;
    delete query.dateMax;
  }
  return filter;
};

const handleRequest = async (req, res) => {
  try {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, flow-id, track-id, request-id, application, apikey, token');

    res.writeHead(200, { 'Content-Type': 'application/json' });

    const path = url.parse(req.url).pathname;
    const { query } = url.parse(req.url, true);

    let data;
    let filter;
    switch (path) {
      case '/':
        responseData(API_WORKING, res);
        break;

      case '/functionOptions':
        data = currency.getFunctionOptions();
        responseData(data, res);
        break;

      case '/symbolOptions':
        data = currency.getSymbolOptions();
        responseData(data, res);
        break;

      case '/marketOptions':
        data = currency.getMarketOptions();
        responseData(data, res);
        break;

      case '/currencyHistory':
        if (!validateParams(query)) {
          responseData(API_BAD_PARAMS, res);
          break;
        }
        filter = validateFilters(query);
        switch (query.functions) {
          case 'DIGITAL_CURRENCY_INTRADAY':
            data = await currency.getCurrencyHistoryDaily(query);
            data = utils.filterResponse(data, filter, 'INTRADAY');
            break;
          case 'DIGITAL_CURRENCY_MONTHLY':
            data = await currency.getCurrencyHistoryMonthly(query);
            data = utils.filterResponse(data, filter, 'MONTHLY');
            break;
          default:
            data = await currency.getCurrencyHistory(query);
        }
        responseData(data, res);
        break;

      default:
        res.writeHead(404, { 'Content-Type': 'application/json' });
        responseData(API_NOT_FOUND, res);
    }
  } catch (err) {
    logger.error(err);
    res.writeHead(400);
    responseData(err, res);
  }

  // Handle Error
  req.on('error', (err) => {
    logger.error(err);
    res.writeHead(400);
    responseData(API_BAD_REQUEST, res);
  });

  res.on('error', (err) => {
    logger.error(err);
    res.writeHead(400);
    responseData(API_BAD_REQUEST, res);
  });
};

module.exports = {
  handleRequest,
};
