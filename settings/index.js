const path = require('path');
const nconf = require('nconf');
const yaml = require('js-yaml');

const ENV = process.env.NODE_ENV || 'development';

nconf.argv();
nconf.env();
nconf.defaults({
  NODE_ENV: ENV,
});

nconf.file(ENV, {
  file: path.join(__dirname, `./${ENV}.yml`),
  format: {
    parse: yaml.safeLoad,
    stringify: yaml.safeDump,
  },
  logicalSeparator: '__',
});
