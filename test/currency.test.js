const axios = require('axios');
const { expect } = require('chai');

const server = require('../app');

const alphaConfig = {
  timeout: 10000,
  baseURL: 'http://localhost:3001/',
};

const alphaRequest = axios.create(alphaConfig);

describe('Verifiy status and content from API', () => {
  it('status', async () => {
    const response = await alphaRequest.get();
    expect(response.status).to.equal(200);
  });
  it('content', async () => {
    const { data } = await alphaRequest.get();
    expect(data).to.equal('API is working!');
  });
});

describe('Getting all functions options and should be an array data', () => {
  it('data type and length', async () => {
    const { data } = await alphaRequest.get('/functionOptions');
    expect(data).to.be.an.instanceof(Array).and.have.lengthOf(4);
  });
});

describe('Getting all symbol options and should contain an object with properties name and value', () => {
  it('data type and value', async () => {
    const { data } = await alphaRequest.get('/symbolOptions');
    expect(data[0]).to.be.an.instanceof(Object).and.have.keys('name', 'value');
  });
});

describe('Getting all markets options and should contain an object with properties name and value', () => {
  it('data type and value', async () => {
    const { data } = await alphaRequest.get('/marketOptions');
    expect(data[0]).to.be.an.instanceof(Object).and.have.keys('name', 'value');
  });
});

describe('Should be an Error - check from External API without parameters', () => {
  it('status', async () => {
    const { data } = await alphaRequest.get('/currencyHistory');
    expect(data).to.equal('Parameters is invalid or missing');
  });
});

describe('Get valid Data from External API with Function DIGITAL_CURRENCY_INTRADAY, Symbol BTC and Market USD', async () => {
  it('status and getting data', async () => {
    const response = await alphaRequest.get('/currencyHistory', {
      params: {
        functions: 'DIGITAL_CURRENCY_INTRADAY',
        symbol: 'BTC',
        market: 'USD',
        apikey: 'MMBJNWF19WLEPXT4',
      },
    });
    expect(response.status).to.equal(200);
    expect(response.data[0]).to.be.an.instanceof(Object);
  });
});

describe('Get valid Data from External API with Function DIGITAL_CURRENCY_MONTHLY, Symbol BTC and Market CNY', () => {
  it('getting all data', async () => {
    const response = await alphaRequest.get('/currencyHistory', {
      params: {
        functions: 'DIGITAL_CURRENCY_MONTHLY',
        symbol: 'BTC',
        market: 'CNY',
        apikey: 'MMBJNWF19WLEPXT4',
      },
    });
    expect(response.status).to.equal(200);
    expect(response.data[0]).to.be.an.instanceof(Object);
  });
});
